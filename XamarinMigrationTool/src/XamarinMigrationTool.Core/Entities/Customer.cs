using SQLite;

namespace XamarinMigrationTool.Core.Entities
{
    public class Customer
    {
        [AutoIncrement]
        [PrimaryKey]
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string CustomerType { get; set; }
    }
}
