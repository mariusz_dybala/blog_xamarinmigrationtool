using System.Threading.Tasks;
using XamarinMigrationTool.Core.Entities;
using XamarinMigrationTool.Core.Services.Interfaces;
using XamarinMigrationTool.Core.Utils;

namespace XamarinMigrationTool.Core.Migrations
{
    [Migration(1)]
    public class InitCustomerTableMigration : IDatabaseMigration
    {
        private readonly IUnitOfWork _unitOfWork;
        public InitCustomerTableMigration(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task ExecuteAsync()
        {
            var connection = await _unitOfWork.GetConnectionAsync();

            await connection.DropTableAsync<Customer>();
            await connection.CreateTableAsync<Customer>();

            var cusomerKrakow = new Customer {City = "Krakow", Name = "Mariusz Dybala"};
            var cusomerNewYork = new Customer {City = "New York", Name = "Chandler Bing"};
            var cusomerLondon = new Customer {City = "London", Name = "Tom York"};

            await connection.InsertAsync(cusomerKrakow);
            await connection.InsertAsync(cusomerNewYork);
            await connection.InsertAsync(cusomerLondon);

            await connection.CloseAsync();

        }
    }
}
