using System.Threading.Tasks;
using XamarinMigrationTool.Core.Entities;
using XamarinMigrationTool.Core.Services.Interfaces;
using XamarinMigrationTool.Core.Utils;

namespace XamarinMigrationTool.Core.Migrations
{
    [Migration(2)]
    public class SetCustomerTypeForCustomersMigration : IDatabaseMigration
    {
        private readonly IUnitOfWork _unitOfWork;
        public SetCustomerTypeForCustomersMigration(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task ExecuteAsync()
        {
            var connection = await _unitOfWork.GetConnectionAsync();

            var customers = await connection.Table<Customer>().ToListAsync();

            foreach (var customer in customers)
            {
                customer.CustomerType = customer.City == "Krakow" ? "Local" : "External";

                await connection.UpdateAsync(customer);
            }

            var customersUpdated = await connection.Table<Customer>().ToListAsync();

            await connection.CloseAsync();
        }
    }
}
