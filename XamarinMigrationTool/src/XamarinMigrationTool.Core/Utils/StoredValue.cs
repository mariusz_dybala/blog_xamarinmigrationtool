namespace XamarinMigrationTool.Core.Utils
{
    public class StoredValue<T>
    {
        public bool ValueExists { get; set; }
        public T Value { get; set; }

        public StoredValue(bool valueExists, T value)
        {
            ValueExists = valueExists;
            Value = value;
        }
    }
}
