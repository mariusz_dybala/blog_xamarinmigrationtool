﻿using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using XamarinMigrationTool.Core.Services;
using XamarinMigrationTool.Core.Services.Interfaces;
using XamarinMigrationTool.Core.ViewModels.Home;

namespace XamarinMigrationTool.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<HomeViewModel>();
            Mvx.IoCProvider.ConstructAndRegisterSingleton<IDatabaseInfoProvider, DatabaseInfoProvider>();
            Mvx.IoCProvider.ConstructAndRegisterSingleton<IUnitOfWork, UnitOfWork>();
        }
    }
}
