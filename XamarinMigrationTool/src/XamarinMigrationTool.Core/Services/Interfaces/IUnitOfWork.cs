using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using XamarinMigrationTool.Core.Entities;

namespace XamarinMigrationTool.Core.Services.Interfaces
{
    public interface IUnitOfWork
    {
        Task<SQLiteAsyncConnection> GetConnectionAsync();
        Task<List<Customer>> GetItemsAsync();
        Task<Customer> GetItemAsync(int id);
        Task<int> SaveItemAsync(Customer item);
    }
}
