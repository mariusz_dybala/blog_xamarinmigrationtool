using System.Threading.Tasks;

namespace XamarinMigrationTool.Core.Services.Interfaces
{
    public interface IDatabaseMigration
    {
        Task ExecuteAsync();
    }
}
