using SQLite;

namespace XamarinMigrationTool.Core.Services.Interfaces
{
    public interface IDatabaseInfoProvider
    {
        string DatabaseFilePath { get;}
        SQLiteOpenFlags Flags { get; }
    }
}
