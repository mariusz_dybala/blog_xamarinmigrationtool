using System.Threading.Tasks;

namespace XamarinMigrationTool.Core.Services.Interfaces
{
    public interface IDatabaseMigrator
    {
        Task ExecuteMigrationsIfNeededAsync();
    }
}
