using XamarinMigrationTool.Core.Utils;

namespace XamarinMigrationTool.Core.Services.PlatformInterfaces
{
    public interface IKeyValueStoreService
    {
        bool SetValue(string key, string value);
        StoredValue<T> GetValueOrDefault<T>(string key);
    }
}
