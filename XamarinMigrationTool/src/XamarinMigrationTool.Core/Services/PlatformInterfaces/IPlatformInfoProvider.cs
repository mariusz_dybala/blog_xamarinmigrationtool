using System;
using System.Collections.Generic;

namespace XamarinMigrationTool.Core.Services.PlatformInterfaces
{
    public interface IPlatformInfoProvider
    {
        IEnumerable<Type> GetAssembliesForType<T>();
    }
}
