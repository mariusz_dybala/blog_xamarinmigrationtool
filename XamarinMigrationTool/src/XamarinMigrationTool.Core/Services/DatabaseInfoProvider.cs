using System;
using System.IO;
using SQLite;
using XamarinMigrationTool.Core.Services.Interfaces;

namespace XamarinMigrationTool.Core.Services
{
    public class DatabaseInfoProvider : IDatabaseInfoProvider
    {
        private string _dataBaseFileName = "maindb.db3";

        public SQLiteOpenFlags Flags => SQLiteOpenFlags.ReadWrite |
                                              SQLiteOpenFlags.Create |
                                              SQLiteOpenFlags.SharedCache;

        public string DatabaseFilePath =>
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), _dataBaseFileName);
    }
}
