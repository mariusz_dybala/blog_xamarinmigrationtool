using System;
using MvvmCross;
using MvvmCross.IoC;
using SQLite;
using XamarinMigrationTool.Core.Entities;
using XamarinMigrationTool.Core.Services.Interfaces;
using XamarinMigrationTool.Core.Utils;

namespace XamarinMigrationTool.Core.Services
{
    public class DatabaseInitializer
    {
        private readonly IDatabaseInfoProvider _databaseInfoProvider;
        public static SQLiteAsyncConnection Connection;

        private DatabaseInitializer(IDatabaseInfoProvider databaseInfoProvider)
        {
            _databaseInfoProvider = databaseInfoProvider;
            Connection = new SQLiteAsyncConnection(_databaseInfoProvider.DatabaseFilePath, _databaseInfoProvider.Flags);
        }

        public static readonly AsyncLazy<DatabaseInitializer> Instance = new AsyncLazy<DatabaseInitializer>(async () =>
        {
            var _sqlDataProvider = Mvx.IoCProvider.Resolve<IDatabaseInfoProvider>();
            var instance = new DatabaseInitializer(_sqlDataProvider);
            CreateTableResult result = await Connection.CreateTableAsync<Customer>();
            return instance;
        });
    }
}
