using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using XamarinMigrationTool.Core.Entities;
using XamarinMigrationTool.Core.Services.Interfaces;

namespace XamarinMigrationTool.Core.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        public async Task<SQLiteAsyncConnection> GetConnectionAsync()
        {
            await DatabaseInitializer.Instance;
            return DatabaseInitializer.Connection;
        }

        public async Task<List<Customer>> GetItemsAsync()
        {
            var connection = await GetConnectionAsync();
            return await connection.Table<Customer>().ToListAsync();
        }

        public async Task<Customer> GetItemAsync(int id)
        {
            var connection = await GetConnectionAsync();
            return await connection.Table<Customer>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<int> SaveItemAsync(Customer item)
        {
            var connection = await GetConnectionAsync();
            if (item.Id != 0)
            {
                return await connection.UpdateAsync(item);
            }

            return await connection.InsertAsync(item);
        }
    }
}
