using Foundation;
using XamarinMigrationTool.Core.Services.PlatformInterfaces;
using XamarinMigrationTool.Core.Utils;

namespace XamarinMigrationTool.iOS.Implementations
{
    public class KeyValueStoreService : IKeyValueStoreService
    {
        public StoredValue<T> GetValueOrDefault<T>(string key)
        {
            var value = NSUserDefaults.StandardUserDefaults.StringForKey(key);

            if (string.IsNullOrWhiteSpace(value))
            {
                return new StoredValue<T>(false, default);
            }

            if (value is T valueCasted)
            {
                return new StoredValue<T>(true, valueCasted);
            }

            return new StoredValue<T>(false, default);
        }

        public bool SetValue(string key, string value)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return false;
            }

            NSUserDefaults.StandardUserDefaults.SetString(value, key);
            return NSUserDefaults.StandardUserDefaults.Synchronize();
        }
    }
}
