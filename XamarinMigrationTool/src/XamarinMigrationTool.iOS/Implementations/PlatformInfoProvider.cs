using System;
using System.Collections.Generic;
using System.Reflection;
using XamarinMigrationTool.Core.Services.PlatformInterfaces;

namespace XamarinMigrationTool.iOS.Implementations
{
    public class PlatformInfoProvider : IPlatformInfoProvider
    {
        public IEnumerable<Type> GetAssembliesForType<T>()
        {
            var assemblyType = typeof(T).GetTypeInfo().Assembly;
            return assemblyType.GetTypes();
        }
    }
}
