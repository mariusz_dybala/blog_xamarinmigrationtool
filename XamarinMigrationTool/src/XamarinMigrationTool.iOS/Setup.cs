using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Forms.Platforms.Ios.Core;
using MvvmCross.IoC;
using Serilog;
using Serilog.Extensions.Logging;
using XamarinMigrationTool.Core.Services;
using XamarinMigrationTool.Core.Services.Interfaces;
using XamarinMigrationTool.Core.Services.PlatformInterfaces;
using XamarinMigrationTool.iOS.Implementations;

namespace XamarinMigrationTool.iOS
{
    public class Setup : MvxFormsIosSetup<Core.App, UI.App>
    {
        protected override ILoggerProvider CreateLogProvider() => new SerilogLoggerProvider();

        protected override ILoggerFactory CreateLogFactory()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.NSLog()
                .CreateLogger();

            return new SerilogLoggerFactory();
        }

        protected override async void InitializeLastChance(IMvxIoCProvider iocProvider)
        {
            Mvx.IoCProvider.ConstructAndRegisterSingleton<IKeyValueStoreService, KeyValueStoreService>();
            Mvx.IoCProvider.ConstructAndRegisterSingleton<IPlatformInfoProvider, PlatformInfoProvider>();
            Mvx.IoCProvider.ConstructAndRegisterSingleton<IDatabaseMigrator, DatabaseMigrator>();

            var migrator = iocProvider.Resolve<IDatabaseMigrator>();

            await migrator.ExecuteMigrationsIfNeededAsync();
        }
    }
}
